package com.intech.desktop;


import javafx.animation.Animation;
import javafx.animation.RotateTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

public class DesktopController implements Initializable {
    private static boolean option1 = false;
    private static boolean option2 = false;
    private float counter;
    @FXML
    private Label bitcoinLabel;

    @FXML
    Button tech1;

    @FXML
    Button cryptoButton;

    @FXML
    ImageView bitcoin;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // initialize is executed one time at the begining
        runThread();
    }

    @FXML
    public void handle() {
        option1 = true; // activate
    }

    //Method to execute autoclick method every 2s
    private void runThread(){
        Timer timer = new java.util.Timer();
            timer.schedule(new TimerTask() {
                public void run() {
                    Platform.runLater(() -> {
                        if(option1) {
                            autoclick();
                        }
                        if(option2) {
                            // to do
                        }

                    });
                }
            }, 0, 1000L);

    }

    //Event to increment counter when button is clicked
    @FXML
    public void buttonHandler(ActionEvent actionEvent) {
        counter = counter + 1.0f;
        bitcoinLabel.setText("Bitcoin " + counter + "");
    }


    private void autoclick() {
        counter = counter + 0.1f;
        bitcoinLabel.setText("Bitcoin " + counter + "");
    }

    @FXML
    public void rotateBigButton(){
        RotateTransition rotateTransition = new RotateTransition();
        rotateTransition.setNode(bitcoin);


        cryptoButton.setOnMouseEntered(mouseEvent -> {
            System.out.println("entered");
            rotateTransition.setToAngle(360);
            rotateTransition.setCycleCount(Animation.INDEFINITE);
            rotateTransition.setDuration(Duration.seconds(1));
            rotateTransition.play();
        });
        cryptoButton.setOnMouseExited(mouseEvent -> {
            System.out.println("exited");
            rotateTransition.pause();
        });

    }

    @Override
    public String toString(){
        return this.counter+"";
    }

    public void setCounter(float counter) {
        this.counter = counter;
    }

    public float getCounter() {
        return counter;
    }

}