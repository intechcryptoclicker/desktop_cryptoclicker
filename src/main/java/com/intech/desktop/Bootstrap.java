package com.intech.desktop;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Pair;
import org.json.simple.JSONObject;

import java.util.Arrays;
import java.util.List;

public class Bootstrap extends Application {
    DesktopController controllerData = new DesktopController();
    SaveData saveData = new SaveData();
    DesktopController thread = new DesktopController();


    @Override
    public void start(Stage stage) throws Exception {
        //Menu

        //Settings window
        stage.setTitle("CryptoClicker");
        stage.setWidth(800);
        stage.setHeight(600);
        stage.getIcons().add(new Image("iconCryptoclicker.png"));

        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("desktop.fxml"));
        Parent root = loader.load();
        Scene mainScene = new Scene(root);
        mainScene.getStylesheets().add(getClass().getClassLoader().getResource("style.css").toString());

        //Initialize counter then assign value found in data.json
        float counter = 0;
        try {
            JSONObject jsonObject = (JSONObject) SaveData.loadData("data.json");
            Double value =  (Double) jsonObject.get("Bitcoin");
            counter = value.floatValue();
        } catch (Exception e){
            e.printStackTrace();
        }

        DesktopController controller = loader.getController();
        controller.setCounter(counter);
        //controller.tech1.setOnAction(actionEvent -> controller.threadHandler());
        stage.setScene(mainScene);
        stage.show();

        //Save data when app is requested to close
        stage.setOnCloseRequest(windowEvent -> {
            try {
                saveData.writeData("data.json", controller);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Platform.exit();
            System.exit(0);
        });

    }

    public static void bootstrap(String[] args){
        launch(args);

    }

}